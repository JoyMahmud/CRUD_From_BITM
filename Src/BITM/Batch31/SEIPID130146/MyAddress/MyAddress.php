<?php
namespace App\BITM\Batch31\SEIPID130146\MyAddress;

use App\BITM\Batch31\SEIPID130146\Utility\Utility;
use App\BITM\Batch31\SEIPID130146\Message\Message;
use PDO;

class MyAddress
{
    public $id = "";
    public $first_name = "";
    public $last_name = "";
    public $mobile_No = "";
    public $email = "";
    public $serverName = "localhost";
    public $databaseName = "myself";
    public $user = "root";
    public $pass = "";
    public $conn;
    public $delete_at = "";

    public function __construct()
    {
        try {
            $this->conn = new PDO("mysql:host=$this->serverName;dbname=$this->databaseName", $this->user, $this->pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function setData($data = "")
    {
        if (array_key_exists('firstName', $data) and !empty($data)) {
            $this->first_name = $data['firstName'];
        }
        if (array_key_exists('lastName', $data) and !empty($data)) {
            $this->last_name = $data['lastName'];
        }
        if (array_key_exists('mobileNo', $data) and !empty($data)) {
            $this->mobile_No = $data['mobileNo'];
        }
        if (array_key_exists('email', $data) and !empty($data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('id', $data) and !empty($data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store()
    {
        $query = "INSERT INTO myaddress (firstName,lastName,mobileNo,email) VALUES (:first_name,:last_name,:mobile_No,:email)";
        $stmt = $this->conn->prepare($query);
        $result = $stmt->execute(array(':first_name' => $this->first_name,
            ':last_name' => $this->last_name, ':mobile_No' => $this->mobile_No, ':email' => $this->email
        ));

        if ($result) {
            //header('location:index.php');
            Message::message("<div class=\"alert alert-success\"><strong>Success!</strong>Data has been inserted successfully</div>");
            Utility::redirect('index.php');
        } else {
            Message::message("<div class=\"alert alert-danger\"><strong>Error!</strong>Some error has been occure</div>");
            Utility::redirect('index.php');
        }
    }


    public function index()
    {
        $sqlquery = "SELECT * FROM `myaddress` WHERE `delete_at` IS NULL";
        $stmt = $this->conn->query($sqlquery);
        $_alldata = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $_alldata;
    }

    public function view()
    {
        $sqlquery = "SELECT * FROM `myaddress` WHERE `id`=:setid";
        $stmt = $this->conn->prepare($sqlquery);
        $stmt->execute(array(':setid' => $this->id
        ));
        $singledata = $stmt->fetch(PDO::FETCH_OBJ);
        return $singledata;
    }

    public function update()
    {
        $udtquery = "UPDATE `myself`.`myaddress` SET `firstName` = :firstName, `lastName` = :lastName, `mobileNo` = :mobileNo, `email` = :email WHERE `myaddress`.`id` = :id;";
        $stmt = $this->conn->prepare($udtquery);
        $result = $stmt->execute(array(':firstName' => $this->first_name,
            ':lastName' => $this->last_name, ':mobileNo' => $this->mobile_No, ':email' => $this->email, ':id'=>$this->id
        ));

        if ($result) {
            //header('location:index.php');
            Message::message("<div class=\"alert alert-success\"><strong>Success!</strong>Data has been Updated successfully</div>");
            Utility::redirect('index.php');
        } else {
            Message::message("<div class=\"alert alert-danger\"><strong>Error!</strong>Some error has been occure</div>");
            Utility::redirect('index.php');
        }
    }


    public function delete(){
        //$dltquery = "DELETE FROM `myself`.`myaddress` WHERE `myaddress`.`id` = :id;";
        $dltquery = "DELETE FROM `myaddress` WHERE `myaddress`.`id` = :id";
        $stmt = $this->conn->prepare($dltquery);
        $result = $stmt->execute(array(
            ':id'=>$this->id
        ));

        if ($result) {
            //header('location:index.php');
            Message::message("<div class=\"alert alert-success\"><strong>Success!</strong>Data has been Deleted successfully</div>");
            Utility::redirect('index.php');
        } else {
            Message::message("<div class=\"alert alert-danger\"><strong>Error!</strong>Some error has been occure</div>");
            Utility::redirect('index.php');
        }
    }

    public function trash()
    {
        $this->delete_at = date("Y-m-d");
        //$udtquery = "UPDATE `myself`.`myaddress` SET `delete_at` = :delete_at WHERE `myaddress`.`id` = :id;";
        $udtquery = "UPDATE `myself`.`myaddress` SET `delete_at` = :delete_at WHERE `myaddress`.`id` = :id";
        $stmt = $this->conn->prepare($udtquery);
        $result = $stmt->execute(array(':delete_at' => $this->delete_at,
            ':id'=>$this->id
        ));

        if ($result) {
            //header('location:index.php');
            Message::message("<div class=\"alert alert-success\"><strong>Success!</strong>Data has been Trashed successfully</div>");
            Utility::redirect('index.php');
        } else {
            Message::message("<div class=\"alert alert-danger\"><strong>Error!</strong>Some error has been occure</div>");
            Utility::redirect('index.php');
        }
    }

    public function trashed()
    {
        $sqlquery = "SELECT * FROM `myaddress` WHERE `delete_at` IS NOT NULL";
        $stmt = $this->conn->query($sqlquery);
        $_alldata = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $_alldata;
    }

    public function restore()
    {
        $this->delete_at = NULL;
        //$udtquery = "UPDATE `myself`.`myaddress` SET `delete_at` = :delete_at WHERE `myaddress`.`id` = :id;";
        $udtquery = "UPDATE `myself`.`myaddress` SET `delete_at` = :delete_at WHERE `myaddress`.`id` = :id";
        $stmt = $this->conn->prepare($udtquery);
        $result = $stmt->execute(array(':delete_at' => $this->delete_at,
            ':id'=>$this->id
        ));

        if ($result) {
            //header('location:index.php');
            Message::message("<div class=\"alert alert-success\"><strong>Success!</strong>Data has been Restored successfully</div>");
            Utility::redirect('index.php');
        } else {
            Message::message("<div class=\"alert alert-danger\"><strong>Error!</strong>Some error has been occure</div>");
            Utility::redirect('index.php');
        }
    }
}












