<?php
namespace App\BITM\Batch31\SEIPID130146\Hobby;

use App\BITM\Batch31\SEIPID130146\Utility\Utility;
use App\BITM\Batch31\SEIPID130146\Message\Message;
use PDO;

class Hobby
{
    public $id = "";
    public $name = "";
    public $hobby = "";
    public $serverName = "localhost";
    public $databaseName = "myself";
    public $user = "root";
    public $pass = "";
    public $conn;
    public $delete_at = "";

    public function __construct()
    {
        try {
            $this->conn = new PDO("mysql:host=$this->serverName;dbname=$this->databaseName", $this->user, $this->pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function setData($data = "")
    {
        if (array_key_exists('name', $data) and !empty($data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('hobby', $data) and !empty($data)) {
            $this->hobby = $data['hobby'];
        }
        if (array_key_exists('id', $data) and !empty($data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

   public function store()
    {
        $query = "INSERT INTO myhobby (name,hobby) VALUES (:name,:hobby)";
        $stmt = $this->conn->prepare($query);
        $result = $stmt->execute(array(':name' => $this->name,
            ':hobby' => $this->hobby
        ));

        if ($result) {
            //header('location:index.php');
            Message::message("<div class=\"alert alert-success\"><strong>Success!</strong>Data has been inserted successfully</div>");
            Utility::redirect('index.php');
        } else {
            Message::message("<div class=\"alert alert-danger\"><strong>Error!</strong>Some error has been occure</div>");
            Utility::redirect('index.php');
        }
    }


     public function index()
     {
         $sqlquery = "SELECT * FROM `myhobby` WHERE `delete_at` IS NULL";
         $stmt = $this->conn->query($sqlquery);
         $_alldata = $stmt->fetchAll(PDO::FETCH_OBJ);
         return $_alldata;
     }

     public function view()
     {
         $sqlquery = "SELECT * FROM `myhobby` WHERE `id`=:setid";
         $stmt = $this->conn->prepare($sqlquery);
         $stmt->execute(array(':setid' => $this->id
         ));
         $singledata = $stmt->fetch(PDO::FETCH_OBJ);
         return $singledata;
     }

     public function update()
     {
         $udtquery = "UPDATE `myself`.`myhobby` SET `name` = :name, `hobby` = :hobby WHERE `myhobby`.`id` = :id";
         $stmt = $this->conn->prepare($udtquery);
         $result = $stmt->execute(array(':name' => $this->name,
             ':hobby' => $this->hobby, ':id'=>$this->id
         ));

         if ($result) {
             //header('location:index.php');
             Message::message("<div class=\"alert alert-success\"><strong>Success!</strong>Data has been Updated successfully</div>");
             Utility::redirect('index.php');
         } else {
             Message::message("<div class=\"alert alert-danger\"><strong>Error!</strong>Some error has been occure</div>");
             Utility::redirect('index.php');
         }
     }


     public function delete(){
         //$dltquery = "DELETE FROM `myself`.`myaddress` WHERE `myaddress`.`id` = :id;";
         $dltquery = "DELETE FROM `myhobby` WHERE `myhobby`.`id` = :id";
         $stmt = $this->conn->prepare($dltquery);
         $result = $stmt->execute(array(
             ':id'=>$this->id
         ));

         if ($result) {
             //header('location:index.php');
             Message::message("<div class=\"alert alert-success\"><strong>Success!</strong>Data has been Deleted successfully</div>");
             Utility::redirect('index.php');
         } else {
             Message::message("<div class=\"alert alert-danger\"><strong>Error!</strong>Some error has been occure</div>");
             Utility::redirect('index.php');
         }
     }

     public function trash()
     {
         $this->delete_at = date("Y-m-d");
         //$udtquery = "UPDATE `myself`.`myaddress` SET `delete_at` = :delete_at WHERE `myaddress`.`id` = :id;";
         $udtquery = "UPDATE `myself`.`myhobby` SET `delete_at` = :delete_at WHERE `myhobby`.`id` = :id";
         $stmt = $this->conn->prepare($udtquery);
         $result = $stmt->execute(array(':delete_at' => $this->delete_at,
             ':id'=>$this->id
         ));

         if ($result) {
             //header('location:index.php');
             Message::message("<div class=\"alert alert-success\"><strong>Success!</strong>Data has been Trashed successfully</div>");
             Utility::redirect('index.php');
         } else {
             Message::message("<div class=\"alert alert-danger\"><strong>Error!</strong>Some error has been occure</div>");
             Utility::redirect('index.php');
         }
     }

    public function trashed()
    {
        $sqlquery = "SELECT * FROM `myhobby` WHERE `delete_at` IS NOT NULL";
        $stmt = $this->conn->query($sqlquery);
        $_alldata = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $_alldata;
    }
     public function restore()
     {
         $this->delete_at = NULL;
         //$udtquery = "UPDATE `myself`.`myaddress` SET `delete_at` = :delete_at WHERE `myaddress`.`id` = :id;";
         $udtquery = "UPDATE `myself`.`myhobby` SET `delete_at` = :delete_at WHERE `myhobby`.`id` = :id";
         $stmt = $this->conn->prepare($udtquery);
         $result = $stmt->execute(array(':delete_at' => $this->delete_at,
             ':id'=>$this->id
         ));

         if ($result) {
             //header('location:index.php');
             Message::message("<div class=\"alert alert-success\"><strong>Success!</strong>Data has been Restored successfully</div>");
             Utility::redirect('index.php');
         } else {
             Message::message("<div class=\"alert alert-danger\"><strong>Error!</strong>Some error has been occure</div>");
             Utility::redirect('index.php');
         }
     }
}












