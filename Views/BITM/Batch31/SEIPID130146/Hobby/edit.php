<?php
include_once ('../../../../../vendor/autoload.php');
use App\BITM\Batch31\SEIPID130146\Hobby\Hobby;
use App\BITM\Batch31\SEIPID130146\Utility\Utility;
//var_dump($_GET);
$obj4 = new Hobby();
 $obj4->setData($_GET);
$data = $obj4->view();
//var_dump($data['hobby']);
$data1=explode(',',$data->hobby);


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Your Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <a href="index.php" class="btn btn-success" role="button">Don't Want Update</a>

    <h2>Update Your Address</h2>
    <form action="update.php" method="post">
        <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $data->id ?>">

            <label for="name">Your Name</label>
            <input type="text" class="form-control" id="name" name="name" value="<?php echo $data->name ?>">

        </div>

        <div class="form-group">
            <label for="order">Hobby:</label>

            <div class="checkbox">
                <label><input type="checkbox" value="Football" name="hobby[]" <?php if (in_array("Football", $data1)) {
                        echo "checked";
                    } ?>>Football</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" value="Cricket" name="hobby[]"<?php if (in_array("Cricket", $data1)) {
                        echo "checked";
                    } ?>>Cricket</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" value="Reading" name="hobby[]"<?php if (in_array("Reading", $data1)) {
                        echo "checked";
                    } ?>>Reading</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" value="Gardening" name="hobby[]"<?php if (in_array("Gardening", $data1)) {
                        echo "checked";
                    } ?>>Gardening</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" value="Dancing" name="hobby[]"<?php if (in_array("Dancing", $data1)) {
                        echo "checked";
                    } ?>>Dancing</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" value="Watching" name="hobby[]"<?php if (in_array("Watching", $data1)) {
                        echo "checked";
                    } ?>>Watching</label>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Update</button>


    </form>
</div>

</body>
</html>
