<?php
include_once ('../../../../../vendor/autoload.php');

use \App\BITM\Batch31\SEIPID130146\Hobby\Hobby;
use App\BITM\Batch31\SEIPID130146\Utility\Utility;

$obj = new Hobby();
$obj->setData($_GET);
$obj->restore();
