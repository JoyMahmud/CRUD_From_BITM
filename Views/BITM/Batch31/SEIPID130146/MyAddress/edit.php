<?php
include_once ('../../../../../vendor/autoload.php');
use App\BITM\Batch31\SEIPID130146\MyAddress\MyAddress;
use App\BITM\Batch31\SEIPID130146\Utility\Utility;

$obj4 = new MyAddress();
$result4 = $obj4->setData($_GET)->view();
//Utility::dd($result);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>About My Self</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <a href="index.php" class="btn btn-success" role="button">Don't Want Update</a>

    <h2>Update Your Address</h2>
    <form action="update.php" method="post">
        <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $result4->id ?>">
            <label for="firstName">First Name</label>
            <input type="text" class="form-control" id="firstName" name="firstName" value="<?php echo $result4->firstName ?>">

        </div>
        <div class="form-group">
            <label for="lastName">Last Name</label>
            <input type="text" class="form-control" id="lastName" name="lastName" value="<?php echo $result4->lastName ?>">
        </div>
        <div class="form-group">
            <label for="mobileNo">Mobile NO</label>
            <input type="number" class="form-control" id="mobileNo" name="mobileNo" value="<?php echo $result4->mobileNo ?>">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" name="email" value="<?php echo $result4->email ?>">
        </div>


        <button type="submit" class="btn btn-primary">Update</button>


    </form>
</div>

</body>
</html>
