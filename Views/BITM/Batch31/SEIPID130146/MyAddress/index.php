<?php
session_start();
include_once ('../../../../../vendor/autoload.php');
use App\BITM\Batch31\SEIPID130146\MyAddress\MyAddress;
use App\BITM\Batch31\SEIPID130146\Utility\Utility;
use App\BITM\Batch31\SEIPID130146\Message\Message;

$obj = new MyAddress();
$allInfo = $obj->index();

//Utility::d();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>About My Information</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div id="success_message">
        <?php
            if (array_key_exists('success_message',$_SESSION) and !empty($_SESSION['success_message'])){

                echo Message::message();

            }

        ?>

    </div>

    <a href="create.php" class="btn btn-primary" role="button">Create Again</a>
    <a href="trashed.php" class="btn btn-success" role="button">All Trashed List</a>


    <h2>All Information</h2>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th><span style="color: green">SL</span></th>
            <th><span style="color: green">ID</span></th>
            <th><span style="color: green">First Name</span></th>
            <th><span style="color: green">Last Name</span></th>
            <th><span style="color: green">Mobile No</span></th>
            <th><span style="color: green">Email</span></th>
            <th><span style="color: green">Action</span></th>
        </tr>
        </thead>
        <tbody>

        <?php
            $sl = 0;
            foreach ($allInfo as $info){
                $sl++;
        ?>

        <tr>
            <td><?php echo $sl; ?></td>
            <td><?php echo $info->id?></td>
            <td><?php echo $info->firstName; ?></td>
            <td><?php echo $info->lastName; ?></td>
            <td><?php echo $info->mobileNo; ?></td>
            <td><?php echo $info->email; ?></td>
            <td>
                <a href="view.php?id=<?php echo $info->id?>" class="btn btn-primary" role="button">View</a>
                <a href="edit.php?id=<?php echo $info->id?>" class="btn btn-success" role="button">Edit</a>
                <a href="delete.php?id=<?php echo $info->id?>" class="btn btn-danger" role="button">Delete</a>
                <a href="trash.php?id=<?php echo $info->id?>" class="btn btn-success" role="button">Trash</a>

               <!-- <form method="post" action="delete.php">
                    <input type="hidden" name="id" value="<?php /*echo $info->id */?>">
                    <button type="submit" class="btn btn-danger" Onclick="return ConfirmDelete();">Delete</button>

                </form>-->
            </td>
        </tr>

        <?php  } ?>

        </tbody>
    </table>
</div>

<script>
    $("#success_message").show().delay(3000).fadeOut();
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }
</script>

</body>
</html>
