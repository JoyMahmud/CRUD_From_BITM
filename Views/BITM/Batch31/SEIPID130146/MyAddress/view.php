<?php
include_once ('../../../../../vendor/autoload.php');
use App\BITM\Batch31\SEIPID130146\MyAddress\MyAddress;
use App\BITM\Batch31\SEIPID130146\Utility\Utility;

$obj3 = new MyAddress();
$result = $obj3->setData($_GET)->view();
//Utility::dd($result);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <a href="index.php" class="btn btn-success" role="button">Back</a>

    <h2>Your Information</h2>
    <ul class="list-group">
        <li class="list-group-item"><strong><span style="color: Green">ID: </span></strong><?php echo $result->id; ?></li>
        <li class="list-group-item"><strong><span style="color: Green">First Name: </span></strong><?php echo $result->firstName; ?></li>
        <li class="list-group-item"><strong><span style="color: Green">Last Name: </span></strong><?php echo $result->lastName; ?></li>
        <li class="list-group-item"><strong><span style="color: Green">Mobile No: </span></strong><?php echo $result->mobileNo; ?></li>
        <li class="list-group-item"><strong><span style="color: Green">Email: </span></strong><?php echo $result->email; ?></li>
    </ul>
</div>

</body>
</html>