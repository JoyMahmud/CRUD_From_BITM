<!DOCTYPE html>
<html lang="en">
<head>
    <title>About My Self</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <a href="index.php" class="btn btn-danger" role="button">Don't Want Create</a>

    <h2>My Address</h2>
    <form action="store.php" method="post">
        <div class="form-group">
            <label for="firstName">First Name</label>
            <input type="text" class="form-control" id="firstName" name="firstName"
                   placeholder="Enter your first name">
        </div>
        <div class="form-group">
            <label for="lastName">Last Name</label>
            <input type="text" class="form-control" id="lastName" name="lastName"
                   placeholder="Enter your Last Name">
        </div>
        <div class="form-group">
            <label for="mobileNo">Mobile NO</label>
            <input type="number" class="form-control" id="mobileNo" name="mobileNo"
                   placeholder="Enter your mobile no">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" name="email"
                   placeholder="Enter your email">
        </div>


        <button type="submit" class="btn btn-primary">Submit</button>



    </form>
</div>

</body>
</html>
