<?php
include_once ('../../../../../vendor/autoload.php');

use \App\BITM\Batch31\SEIPID130146\MyAddress\MyAddress;
use App\BITM\Batch31\SEIPID130146\Utility\Utility;

$obj = new MyAddress();
$obj->setData($_POST);
$obj->store();
